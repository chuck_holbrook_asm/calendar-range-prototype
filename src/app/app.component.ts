import { element } from 'protractor';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { MatCalendarCellCssClasses, MatCalendar, MatCalendarHeader } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'calendar-range';
  test: boolean = true;
  selectedDate: any;
  d1: any = new Date(new Date().setDate(new Date(new Date()).getDate() + 12));
  d2: any = new Date();
  count1: number = 0;
  count2: number = 0;
  datesToHighlight = [];

  constructor() { }

  @ViewChild('primaryCalendar', { 'static': false }) primaryCalendar: MatCalendar<Date>;
  @ViewChild('secondaryCalendar', { 'static': false }) secondaryCalendar: MatCalendar<Date>;


  ngOnInit() {

  }

  syncSecondaryCalendar() {
    const newDate = new Date();
    newDate.setMonth(9);
    this.secondaryCalendar.startAt = newDate;
  }


  onSelect(event, a: HTMLInputElement) {
    console.log(a);
    debugger;
    // this.selectedDate = event;
  }
  rangeChanged(operator, windowType) {
    // console.log(this.primaryCalendar.currentView = 'year');
    if (operator == 'plus') {
      this[windowType]++;
    } else {
      this[windowType]--;
    }
    let tempDate: any = new Date();
    this.datesToHighlight = [];

    if (this[windowType] >= 0) {
      for (let i = 1; i <= (this.count1 + this.count2); i++) {
        if (this.datesToHighlight.length == 0) {
          this.datesToHighlight.push(new Date(this.d1 - 24 * 60 * 60 * 1000));
        } else {
          this.datesToHighlight.push(new Date(this.datesToHighlight[(this.datesToHighlight.length - 1)] - 24 * 60 * 60 * 1000));
        }
      }
    }

    // this.calendar2.monthView.

    if (windowType == 'count1') {
      this.primaryCalendar.monthView.activeDate.setMonth(new Date(this.d1 - ((this.count1) * 24 * 60 * 60 * 1000)).getMonth());
    } else {
      this.primaryCalendar.monthView.activeDate.setMonth(new Date(this.d1 - ((this.count1+this.count2) * 24 * 60 * 60 * 1000)).getMonth());
    }
    // if ((operator == 'plus') && (windowType == 'count1') && (new Date(this.d1 - ((this.count1) * 24 * 60 * 60 * 1000)).getMonth() < new Date(this.d1 - ((this.count1-1) * 24 * 60 * 60 * 1000)).getMonth()) && (this.primaryCalendar.monthView.activeDate.getMonth()+1 != (new Date(this.d1 - ((this.count1) * 24 * 60 * 60 * 1000)).getMonth()))) {
    //   let prevElement:HTMLElement = <HTMLElement>document.getElementsByClassName('mat-primaryCalendar-previous-button')[0];
    //   prevElement.click();
    // }
    // if ((operator == 'minus') && (windowType == 'count1') && (new Date(this.d1 - ((this.count1+1) * 24 * 60 * 60 * 1000)).getMonth() < new Date(this.d1 - ((this.count1) * 24 * 60 * 60 * 1000)).getMonth()) && (this.primaryCalendar.monthView.activeDate.getMonth() != (new Date(this.d1 - ((this.count1) * 24 * 60 * 60 * 1000)).getMonth()))) {
    //   let nextElement:HTMLElement = <HTMLElement>document.getElementsByClassName('mat-primaryCalendar-next-button')[0];
    //   nextElement.click();
    // }
    // if ((operator == 'plus') && (windowType == 'count2') && (new Date(this.d1 - ((this.count1+this.count2) * 24 * 60 * 60 * 1000)).getMonth() < new Date(this.d1 - ((this.count1 + this.count2-1) * 24 * 60 * 60 * 1000)).getMonth()) && (this.primaryCalendar.monthView.activeDate.getMonth()+1 != (new Date(this.d1 - ((this.count1+this.count2) * 24 * 60 * 60 * 1000)).getMonth()))) {
    //   let prevElement:HTMLElement = <HTMLElement>document.getElementsByClassName('mat-primaryCalendar-previous-button')[0];
    //   prevElement.click();
    // }
    // if ((operator == 'minus') && (windowType == 'count2') && (new Date(this.d1 - ((this.count1+this.count2+1) * 24 * 60 * 60 * 1000)).getMonth() < new Date(this.d1 - ((this.count1 + this.count2) * 24 * 60 * 60 * 1000)).getMonth()) && (this.primaryCalendar.monthView.activeDate.getMonth() != (new Date(this.d1 - ((this.count1+this.count2) * 24 * 60 * 60 * 1000)).getMonth()))) {
    //   let nextElement:HTMLElement = <HTMLElement>document.getElementsByClassName('mat-primaryCalendar-next-button')[0];
    //   nextElement.click();
    // }
    // if ((windowType == 'count1') && (new Date(this.d1 - ((this.count1) * 24 * 60 * 60 * 1000)).getMonth() > new Date(this.d1 - ((this.count1-1) * 24 * 60 * 60 * 1000)).getMonth())) {
    //   let prevElement:HTMLElement = document.getElementsByClassName('mat-primaryCalendar-next-button')[0];
    //   prevElement.click();
    // }
    this.primaryCalendar.updateTodaysDate();
    // this.syncSecondaryCalendar();
  }
  dateClass1() {
    return (date: Date): MatCalendarCellCssClasses => {
      const highlightDate = this.datesToHighlight
        .some((d) => {
          return (d.getDate() === date.getDate() && d.getMonth() === date.getMonth() && d.getFullYear() === date.getFullYear());
        }
        );
      if (highlightDate) {
        if (date > new Date(this.d1 - ((this.count1 + 1) * 24 * 60 * 60 * 1000))) {
          return 'special-date1';
        } else {
          return 'special-date2';
        }
      } else {
        if ((date.getDate() == this.d1.getDate()) && (date.getMonth() == this.d1.getMonth()) && (date.getFullYear() == this.d1.getFullYear())) {
          return 'highlight-date';
        } else {
          return '';
        }
      }
    };
  }
}
