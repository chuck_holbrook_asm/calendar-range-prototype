import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import {
  MatButtonModule, MatCheckboxModule,
  MatDatepickerModule, MatNativeDateModule, MatCardModule,
  MatListModule,
  MatTabsModule,
  MatFormFieldModule, MatInputModule, MatIconModule
} from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';
import { AppComponent } from './app.component';
import {RangeCalendarComponent} from './range-calendar/range-calendar.component';

@NgModule({
  declarations: [
    AppComponent,
    RangeCalendarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    AppRoutingModule,
    MatDatepickerModule, MatNativeDateModule,
    MatCardModule,
    MatListModule,
    MatTabsModule,
    MatFormFieldModule, MatInputModule,
    FlexLayoutModule
  ],
  providers: [],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
