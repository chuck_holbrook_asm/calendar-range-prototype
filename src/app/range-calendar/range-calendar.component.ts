import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, EventEmitter,
  Inject,
  OnDestroy, OnInit, Output, ViewChild
} from '@angular/core';
import {MatCalendar} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS, MatDateFormats} from '@angular/material/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {MatCalendarCellCssClasses} from '@angular/material';
import {NativeDateAdapter} from '@angular/material';
@Component({
  selector: 'app-range-calendar',
  templateUrl: './range-calendar.component.html',
  styleUrls: ['./range-calendar.component.scss']
})
export class RangeCalendarComponent implements OnInit, AfterViewInit, OnDestroy {
  datesToHighlight = [];
  d1: any = new Date(new Date().setDate(new Date(new Date()).getDate() + 12));
  d2: any = new Date();
  count1: number = 0;
  count2: number = 0;


  private _destroyed = new Subject<void>();

  @ViewChild('primaryCalendar', { 'static': false }) primaryCalendar: MatCalendar<Date>;
  @ViewChild('secondaryCalendar', { 'static': false }) secondaryCalendar: MatCalendar<Date>;



  constructor() {

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }


  ngAfterViewInit() {
    const me = this;
    setTimeout(() => {

      // me.primaryCalendar.headerComponent.active => {
      //   me.syncSecondaryCalendar();
      // });

      me.primaryCalendar.stateChanges
        .pipe(takeUntil(this._destroyed))
        .subscribe(() => {
          console.log('State changed');
          me.syncSecondaryCalendar();
        });

      me.primaryCalendar.monthView.activeDateChange.subscribe(() => {
        me.syncSecondaryCalendar();
      });
      me.primaryCalendar.monthSelected.subscribe(() => {
        me.syncSecondaryCalendar();
      });

    });
  }

  eventClicked(event): void {
    console.log( event);
  }

  syncSecondaryCalendar() {
    const newDate = new Date(this.primaryCalendar.activeDate);
    // TODO: Use moment.js to add the month here so that we accomodate year changes too
    newDate.setMonth(newDate.getMonth() + 1);
    this.secondaryCalendar.monthView.activeDate = newDate;
  }




  calendarDateClass() {
    return (date: Date): MatCalendarCellCssClasses => {
      const highlightDate = this.datesToHighlight
        .some((d) => {
            return (d.getDate() === date.getDate() && d.getMonth() === date.getMonth() && d.getFullYear() === date.getFullYear());
          }
        );
      if (highlightDate) {
        if (date > new Date(this.d1 - ((this.count1 + 1) * 24 * 60 * 60 * 1000))) {
          return 'special-date1';
        } else {
          return 'special-date2';
        }
      } else {
        if ((date.getDate() == this.d1.getDate()) && (date.getMonth() == this.d1.getMonth()) && (date.getFullYear() == this.d1.getFullYear())) {
          return 'highlight-date';
        } else {
          return '';
        }
      }
    };
  }
}


//
// /** Custom header component for datepicker. */
// @Component({
//   selector: 'example-header',
//   styles: [`
//     .example-header {
//       display: flex;
//       align-items: center;
//       padding: 0.5em;
//     }
//
//     .example-header-label {
//       flex: 1;
//       height: 1em;
//       font-weight: 500;
//       text-align: center;
//     }
//
//     .example-double-arrow .mat-icon {
//       margin: -22%;
//     }
//   `],
//   template: `
//     <div class="example-header">
//       <button mat-icon-button (click)="previousClicked('month')">
//         <mat-icon>keyboard_arrow_left</mat-icon>
//       </button>
//       <span class="example-header-label">{{periodLabel}}</span>
//       <button mat-icon-button (click)="nextClicked('month')">
//         <mat-icon>keyboard_arrow_right</mat-icon>
//       </button>
//
//     </div>
//   `,
//   changeDetection: ChangeDetectionStrategy.OnPush,
// })
// export class ExampleHeader<D> implements OnDestroy {
//   private _destroyed = new Subject<void>();
//
//   /** Emits when the currently selected date changes. */
//   @Output() activeChanged: EventEmitter<any> = new EventEmitter();
//
//   constructor(
//     private _calendar: MatCalendar<D>, private _dateAdapter: DateAdapter<D>,
//     @Inject(MAT_DATE_FORMATS) private _dateFormats: MatDateFormats, cdr: ChangeDetectorRef) {
//     _calendar.stateChanges
//       .pipe(takeUntil(this._destroyed))
//       .subscribe(() => cdr.markForCheck());
//   }
//
//   ngOnDestroy() {
//     this._destroyed.next();
//     this._destroyed.complete();
//   }
//
//   get periodLabel() {
//     return this._dateAdapter
//       .format(this._calendar.activeDate, this._dateFormats.display.monthYearLabel)
//       .toLocaleUpperCase();
//   }
//
//   previousClicked(mode: 'month' | 'year') {
//     this._calendar.activeDate = mode === 'month' ?
//       this._dateAdapter.addCalendarMonths(this._calendar.activeDate, -1) :
//       this._dateAdapter.addCalendarYears(this._calendar.activeDate, -1);
//
//     this.activeChanged.emit(this._calendar.activeDate);
//   }
//
//   nextClicked(mode: 'month' | 'year') {
//
//
//     this._calendar.activeDate = mode === 'month' ?
//       this._dateAdapter.addCalendarMonths(this._calendar.activeDate, 1) :
//       this._dateAdapter.addCalendarYears(this._calendar.activeDate, 1);
//
//     this.activeChanged.emit(this._calendar.activeDate);
//   }
// }
